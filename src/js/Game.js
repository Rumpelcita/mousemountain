function create() {

    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.setImpactEvents(true);
    game.physics.p2.gravity.y = 150;
    game.physics.p2.restitution = 0;

    cursors = game.input.keyboard.createCursorKeys();
    score = 0;

    micePile = [];
    //  The platforms group contains the ground and the 2 ledges we can jump on
    platforms = game.add.group();
    platforms.enableBody = true;
    platforms.physicsBodyType = Phaser.Physics.P2JS;
    platforms.collitionGroup = game.physics.p2.createCollisionGroup();
    // Mice group
    mice = game.add.group();
    mice.enableBody = true;
    mice.physicsBodyType = Phaser.Physics.P2JS;

    //playerTouch = game.physics.p2.createCollisionGroup();
    miceTouch = game.physics.p2.createCollisionGroup();
    game.physics.p2.setImpactEvents(true);
    game.physics.p2.updateBoundsCollisionGroup();

    // Here we create the ground.
    var ground = platforms.create(0, game.world.height - 26, 'ground');

    var ledge = platforms.create(game.world.width - 28, 130, 'platform');

    //  This stops it from falling away when you jump on it
    game.physics.p2.enable( [ground, ledge]);
    platforms.forEach( function(platform){
        platform.enableBody = true;
        platform.body.static = true;
        platform.body.setCollisionGroup(platforms.collitionGroup);
        platform.body.collides(miceTouch);
    });

    cheese = game.add.sprite(50, 50, 'cheese');
    cheese.anchor.set(0.5, 0.5);
    game.physics.p2.enable(cheese);
    cheese.enableBody = true;
    cheese.physicsBodyType = Phaser.Physics.P2JS;
    cheese.body.static = true;
    cheeseTouch = game.physics.p2.createCollisionGroup();
    cheese.body.setCollisionGroup(cheeseTouch);
    cheese.body.collides(miceTouch, win, cheese);

    // The player and its settings
    player = mice.create(32, game.world.height - 115, 'mouse_white');
    //  We need to enable physics on the player
    //player.physicsBodyType = Phaser.Physics.P2JS;
    game.physics.p2.enable(player);
    player.body.fixedRotation = true;
    player.body.collideWorldBounds = true;
    player.body.setCollisionGroup(miceTouch);
    player.body.collides([miceTouch,platforms.collitionGroup]);
    //Timer for mice spawning
    timer = game.time.create(false);
    timer.loop(3000, spawn_mice, this);
    timer.start();

    }

function update() {

    mice.forEach(function(mouse){
            if (mouse != player){
                if (mouse.body.y > 549){
                    var index = micePile.indexOf(mouse);
                    if (index > -1){
                        micePile.splice(index, 1);
                    }
                    mice.remove(mouse, true);
                    score++
                }
            }
    }
    );
    if (cursors.left.isDown) {
        //  Move to the left
        player.body.moveLeft(95);
        for (var i = 0; i < micePile.length; i++){
            micePile[i].body.moveLeft(95);
        }
    }
    else if (cursors.right.isDown){
        //  Move to the right
        player.body.moveRight(95);
        for (var i = 0; i < micePile.length; i++){
            micePile[i].body.moveRight(95);
        }
    }
}

function render() {
    platforms.forEach( function(platform){
        game.debug.body(platform, 'rgba(255, 255, 0, 0.1)');

    });

}

function spawn_mice() {
    var mouse_type = (Math.floor(Math.random() * 3) + 1);
    if (mouse_type == 1){
        var mouse = mice.create(305, 95, 'mouse_brown');
    } else if (mouse_type == 2) {
        var mouse = mice.create(305, 95, 'mouse_grey');
    } else {
        var mouse = mice.create(305, 95, 'mouse_white');
    }

    mouse.enableBody = true;
    mouse.physicsBodyType = Phaser.Physics.P2JS;
    game.physics.p2.enable(mouse);
    mouse.body.fixedRotation = true;
    mouse.body.collideWorldBounds = true;
    mouse.body.setCollisionGroup(miceTouch);
    mouse.body.collides(miceTouch, move_mouse, mouse);
    mouse.body.collides(platforms.collitionGroup);
    mouse.body.collides(cheeseTouch, win, cheese);
    mouse.body.moveLeft(Math.floor(Math.random() * 60) + 40);
    mouse.body.moveUp(60);
}

function move_mouse(){
    this.body.collides(miceTouch);
    if (micePile.indexOf(this) == -1){
        micePile.push(this);
    }
}

function clean_mouse(){
    this.destroy();
}

function win() {
    this.destroy();
    score += micePile.length;
    game.paused = true;
    var style = { font: "bold 20px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
    var replay_style = { font: "bold 16px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
    win = game.add.sprite(0, 0, 'win');
    win.scale.setTo(2, 2);
    game.add.text(35, game.world.centerY - 50, "The mice got the cheese!", style);
    game.add.text(125, game.world.centerY - 25, "You win!", style);
    game.add.text(35, game.world.centerY, "It took " + score + " mice to make it.", style);
    var playButton = game.add.button(game.world.centerX - 50, game.world.centerY + 55,'retry', menu);
    game.paused = false;
}

function menu(){
    music.stop();
    game.state.start("Menu");
}

var game = new Phaser.Game(360, 625, Phaser.AUTO, '');
game.state.add("Menu", { preload: preload, create: createMenu } );
game.state.add("Game",{ create: create, update: update, render: render });
game.state.start("Menu");
