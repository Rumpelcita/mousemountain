function preload() {
    game.load.image('play', 'assets/img/play.png');
    game.load.audio('music', 'assets/sound/happy_rat.ogg');
    game.load.image('cheese', 'assets/img/cheese.png');
    game.load.image('ground', 'assets/img/ground.png');
    game.load.image('platform', 'assets/img/platform.png');
    game.load.image('mouse_white', 'assets/img/mouse_white.png');
    game.load.image('mouse_grey', 'assets/img/mouse_grey.png');
    game.load.image('mouse_brown', 'assets/img/mouse_brown.png');
    game.load.image('win', 'assets/img/victory.png');
    game.load.image('retry', 'assets/img/retry.png');
}

function createMenu(){
        var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
        var styleInfo = { font: "bold 16px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
        var gameIcon = game.add.sprite(165, 225, 'mouse_white');
        gameIcon.anchor.setTo(0.5,0.5)
        var gameTitle = game.add.text(175, 160, "Cheese Mountain", style);
		gameTitle.anchor.setTo(0.5,0.5);
        var gameInfo = game.add.text(175, 275, "Use the arrows to move", styleInfo);
        gameInfo.anchor.setTo(0.5,0.5);
		var playButton = game.add.button(160,320,'play', playTheGame,this);
		playButton.anchor.setTo(0.5,0.5);
        var gameCredits = game.add.text(15, 475, "Made by Florencia Rumpel Rodriguez\nMusic by Bart Kelsey\n(http://opengameart.org/users/bart)", styleInfo);
        gameInfo.anchor.setTo(0.5,0.5);
        music = game.add.audio('music');
        music.loop = true;
        music.play();
	}

function playTheGame(){
		game.state.start("Game");
	}
